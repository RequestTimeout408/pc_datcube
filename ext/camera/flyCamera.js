// Generated by CoffeeScript 1.3.3
(function() {
  var __hasProp = {}.hasOwnProperty;

  pc.script.create("fly_camera", function(context) {
    var FlyCameraComp;
    FlyCameraComp = (function() {

      function FlyCameraComp(entity) {
        var attribs, componentData, data,
          _this = this;
        this.entity = entity;
        this.camLookAt = pc.math.vec3.create(1, 0, 0);
        this.target = pc.math.vec3.create(0, 0, 0);
        componentData = new pc.fw.CameraComponentData;
        data = {
          camera: this.camera = new FlyCam
        };
        window.fcam = this.camera;
        attribs = ["camera", "clearColor", "fov", "orthoHeight", "activate", "nearClip", "farClip", "offscreen", "projection"];
        context.systems.camera.initialiseComponent(this.entity, componentData, data, attribs);
        context.systems.camera.setCurrent(entity);
        this.camera.translate(100, 200, 10);
        this.camera.lookAt(pc.math.vec3.create(0, 0, 0));
        /*Set up cam listeners
        */

        this.inputs = {};
        this.inputs[pc.input.KEY_W] = function() {
          return _this.camera.dolly(100);
        };
        this.inputs[pc.input.KEY_S] = function() {
          return _this.camera.dolly(-100);
        };
        /*
              @inputs[pc.input.KEY_LEFT]= =>
                @camera.orbit [.05,0]
              @inputs[pc.input.KEY_RIGHT]= =>
                @camera.orbit [-.05,0]
              @inputs[pc.input.KEY_UP]= =>
                @camera.orbit [0,-.05]
              @inputs[pc.input.KEY_DOWN]= =>
                @camera.orbit [0,.05]
        */

        context.mouse.bind(pc.input.EVENT_MOUSE_MOVE, this.camera.onMouseMove.bind(this.camera));
        context.mouse.bind(pc.input.EVENT_MOUSE_WHEEL, this.camera.onMouseWheel.bind(this.camera));
      }

      FlyCameraComp.prototype.update = function(dt) {
        var key, len, value, _ref, _results;
        len = Object.keys(context.keyboard._keymap).length;
        if (len <= 0) {
          return false;
        }
        _ref = this.inputs;
        _results = [];
        for (key in _ref) {
          if (!__hasProp.call(_ref, key)) continue;
          value = _ref[key];
          if (context.keyboard.isPressed(parseInt(key))) {
            _results.push(value.call(this));
          } else {
            _results.push(void 0);
          }
        }
        return _results;
      };

      return FlyCameraComp;

    })();
    return FlyCameraComp;
  });

}).call(this);
